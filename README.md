Install
-------
    sudo npm install -g stylus nib
    mkdir css

Start watcher
-------------
    stylus styl/ --out css/ --watch --use nib
